<?php

return [
    'signin' => 'Por favor, ingrese su usuario y contraseña',
    'email' => 'Correo electrónico',
    'password' => 'Contraseña',
    'singin_button' => 'Acceder',
    'Login' => 'Acceder',
    'E-MailAddress' => 'Correo electrónico',
    'Password' => 'Contraseña',
    'RememberMe' => 'Recuerdame',
    'ForgotYourPassword?' => '¿Has olvidado tu contraseña?',
    'Register'=>'Registro',
    'ResetPassword'=>'Reiniciar contraseña',
    'E-MailAddress'=>'Correo electrónico',
    'Password'=>'Contraseña',
    'ConfirmPassword'=>'Confirmar contraseña',
    'SendPasswordResetLink' => 'Enviar enlace reiniciar contraseña',

];
