<?php

return [
    'signin' => 'Please sign in',
    'email' => 'E-mail Adress',
    'password' => 'Password',
    'singin_button' => 'Sign in',
    'Login' => 'Login',
    'E-Mail Address' => 'E-Mail Address',
    'Password' => 'Password',
    'RememberMe' => 'Remember Me',
    'ForgotYourPassword?' => 'Forgot Your Password?',
    'Register'=>'Register',
    'ResetPassword'=> 'Reset Password',
    'E-MailAddress'=> 'E-Mail Address',
    'Password'=> 'Password',
    'Confirm Password'=> 'Confirm Password',
    'SendPasswordResetLink' => 'Send Password Reset Link',

];
